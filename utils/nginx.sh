#!/usr/bin/env bash

function set_default_nginx_conf () {
    cat > /etc/nginx/nginx.conf <<EOL
# Run as a less privileged user for security reasons.
user www-data;

# How many worker threads to run;
# "auto" sets it to the number of CPU cores available in the system, and
# offers the best performance. Don't set it higher than the number of CPU
# cores if changing this parameter.

# The maximum number of connections for Nginx is calculated by:
# max_clients = worker_processes * worker_connections
worker_processes auto;

# Maximum open file descriptors per process;
# should be > worker_connections.
worker_rlimit_nofile 8192;

pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
  # When you need > 8000 * cpu_cores connections, you start optimizing your OS,
  # and this is probably the point at which you hire people who are smarter than
  # you, as this is *a lot* of requests.
  worker_connections 8000;
}

http {

	# Define the MIME types for files.
	include /etc/nginx/mime.types;
	default_type application/octet-stream;

	# Hide nginx version information.
    server_tokens off;

	 # How long to allow each connection to stay idle; longer values are better
    # for each individual client, particularly for SSL, but means that worker
    # connections are tied up longer. (Default: 65)
    keepalive_timeout 20;

    # Speed up file transfers by using sendfile() to copy directly
    # between descriptors rather than using read()/write().
    sendfile        on;

    # Tell Nginx not to send out partial frames; this increases throughput
    # since TCP frames are filled up before being sent out. (adds TCP_CORK)
    tcp_nopush      on;

	types_hash_max_size 2048;

	server_names_hash_bucket_size 64;
	# server_name_in_redirect off;

	##
	# SSL Settings
	##

	ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3; # Dropping SSLv3, ref: POODLE
	ssl_prefer_server_ciphers on;

	##
	# Logging Settings
	##

	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

	# Compression
    # Enable Gzip compressed.
    gzip on;

    # Compression level (1-9).
    # 5 is a perfect compromise between size and cpu usage, offering about
    # 75% reduction for most ascii files (almost identical to level 9).
    gzip_comp_level    5;

    # Don't compress anything that's already small and unlikely to shrink much
    # if at all (the default is 20 bytes, which is bad as that usually leads to
    # larger files after gzipping).
    gzip_min_length    256;

    # Compress data even for clients that are connecting to us via proxies,
    # identified by the "Via" header (required for CloudFront).
    gzip_proxied       any;

    # Tell proxies to cache both the gzipped and regular version of a resource
    # whenever the client's Accept-Encoding capabilities header varies;
    # Avoids the issue where a non-gzip capable client (which is extremely rare
    # today) would display gibberish if their proxy gave them the gzipped version.
    gzip_vary          on;

    # Compress all output labeled with one of the following MIME-types.
    gzip_types
        application/atom+xml
        application/javascript
        application/json
        application/ld+json
        application/manifest+json
        application/rss+xml
        application/vnd.geo+json
        application/vnd.ms-fontobject
        application/x-font-ttf
        application/x-web-app-manifest+json
        application/xhtml+xml
        application/xml
        font/opentype
        image/bmp
        image/svg+xml
        image/x-icon
        text/cache-manifest
        text/css
        text/plain
        text/vcard
        text/vnd.rim.location.xloc
        text/vtt
        text/x-component
        text/x-cross-domain-policy;

	##
	# Virtual Host Configs
	##

	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;
}


#mail {
#	# See sample authentication script at:
#	# http://wiki.nginx.org/ImapAuthenticateWithApachePhpScript
#
#	# auth_http localhost/auth.php;
#	# pop3_capabilities "TOP" "USER";
#	# imap_capabilities "IMAP4rev1" "UIDPLUS";
#
#	server {
#		listen     localhost:110;
#		protocol   pop3;
#		proxy      on;
#	}
#
#	server {
#		listen     localhost:143;
#		protocol   imap;
#		proxy      on;
#	}
#}
EOL
}

function create_nginx_app () {
    local DOMAIN=$1
    local PHP_VERSION=$2
    local NGINX_APP_PATH=/etc/nginx/sites-available/${DOMAIN}
    cat > "${NGINX_APP_PATH}" <<EOL
server {
    listen 80;
    server_name ${DOMAIN} www.${DOMAIN};
    
    root /var/www/${DOMAIN}/public;
    index index.php;
    
    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/run/php/php${PHP_VERSION}-fpm.sock;
        #fastcgi_pass   127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        fastcgi_param SCRIPT_NAME     \$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }
}

# server {
#     listen 443 ssl http2;
#     listen [::]:443 ssl http2;
#     server_name ${DOMAIN} www.${DOMAIN};

#     add_header X-Frame-Options "SAMEORIGIN";
#     add_header X-XSS-Protection "1; mode=block";

#     # Strengthening security
#     ssl_session_cache shared:SSL:20m;
#     ssl_session_timeout 60m;
#     ssl_prefer_server_ciphers on;
#     ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
#     ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
#     ssl_dhparam /etc/ssl/certs/dhparams.pem;

#     #ssl_certificate /etc/nginx/ssl/self.crt;
#     #ssl_certificate_key /etc/nginx/ssl/self.key;

#     root /var/www/${DOMAIN}/public;
#     index index.php;
    
#     location / {
#         try_files \$uri \$uri/ /index.php?\$query_string;
#     }

#     location ~ \.php$ {
#         fastcgi_split_path_info ^(.+\.php)(/.+)$;
#         fastcgi_pass unix:/run/php/php${PHP_VERSION}-fpm.sock;
#         #fastcgi_pass   127.0.0.1:9000;
#         fastcgi_index index.php;
#         fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
#         fastcgi_param SCRIPT_NAME     \$fastcgi_script_name;
#         include fastcgi_params;
#     }

#     location ~ /\.ht {
#         deny all;
#     }

#     location ~ /.well-known {
#         allow all;
#     }
# }

EOL

    ln -s /etc/nginx/sites-available/${DOMAIN} /etc/nginx/sites-enabled/
    nginx -t
    service nginx reload
}