#!/usr/bin/env bash

function configure_ufw () {
   
    if is_ufw_not_enabled; then
        progress_message "Configuring UFW"

        ufw enable
        
        # Close all incoming ports.
        ufw default deny incoming

        # Open all outgoing ports.
        ufw default allow outgoing

        # Open SSH port.
        ufw allow "${SSH_PORT}/tcp"

        # Open HTTP port.
        ufw allow 80

        # Open HTTPS port.
        ufw allow 443

        # Open DNS port.
        ufw allow 53
    fi
    
    
}


function add_cron {
    crontab -l > mycron
    echo "$1 $2" >> mycron
    crontab mycron
    rm mycron
}

function configure_env () {
    local APP_PATH=$1
    local DB_NAME=$2
    local DB_USER=$3
    local DB_PASS=$4

    cat > "${APP_PATH}/.env" <<EOL
APP_NAME=${LARAVEL_APP_NAME}
APP_ENV=production
APP_KEY=base64:ZLo9ewJ9McR/k3TYsJ3iWMkj7IaBdF8oIhZ+9d2nk3g=
APP_DEBUG=false
APP_URL=https://${LARAVEL_DOMAIN}

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=${DB_NAME}
DB_USERNAME=${DB_USER}
DB_PASSWORD=${DB_PASS}

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DISK=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS="hello@example.com"
MAIL_FROM_NAME="${LARAVEL_APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_HOST=
PUSHER_PORT=443
PUSHER_SCHEME=https
PUSHER_APP_CLUSTER=mt1

VITE_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
VITE_PUSHER_HOST="${PUSHER_HOST}"
VITE_PUSHER_PORT="${PUSHER_PORT}"
VITE_PUSHER_SCHEME="${PUSHER_SCHEME}"
VITE_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

EOL
}

function install_laravel () {
    if is_not_installed "nginx"; then
        throw_error "Please install Nginx"
    fi

    if is_not_installed "php"; then
        throw_error "Please install php"
    fi

    if is_not_installed "composer"; then
        throw_error "Please install compose"
    fi

    if is_not_installed "mysql"; then
        throw_error "Please install Mysql"
    fi

    if is_not_installed "redis-server"; then
        throw_error "Please install redis"
    fi

    configure_ufw

    
    
    # if is_http_failed ""

    local DATABASE=${LARAVEL_DB_NAME:-$(generate_random_string 8)}
    local USERNAME=${LARAVEL_DB_USERNAME:-$(generate_random_username 8)}
    local PASSWORD=${LARAVEL_DB_PASSWORD:-$(openssl rand -base64 64 | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)}
    progress_message "Creating Mysql Database"
    create_mysql_db "root" "${MYSQL_ROOT_PASSWORD}" "${DATABASE}"

    progress_message "Creating Mysql User"
    create_mysql_user "root" "${MYSQL_ROOT_PASSWORD}" "${DATABASE}" "${USERNAME}" "${PASSWORD}"

    if is_http_oke "${LARAVEL_IMPORT_DB_URL}"; then
        progress_message "Importing databaser"
        import_mysql_db "root" "${MYSQL_ROOT_PASSWORD}" "${DATABASE}" $(trim "${LARAVEL_IMPORT_DB_URL}")
    fi

    progress_message "Configuring Nginx"

    local APP_PATH="/var/www/${LARAVEL_DOMAIN}"

    git clone "${LARAVEL_REPO_URL}" "${APP_PATH}"

    configure_env "${APP_PATH}" "${DATABASE}" "${USERNAME}" "${PASSWORD}"

    chown -R www-data: $APP_PATH/storage $APP_PATH/bootstrap

    cd $APP_PATH && composer install

    #create_nginx_app "${LARAVEL_DOMAIN}" "${LARAVEL_PHP_VERSION}"


}

echo ""
while [[ "${DO_INSTALL_LARAVEL}" != "y" && "${DO_INSTALL_LARAVEL}" != "n" ]]; do
    read -rp "Do you want to install Laravel? [y/n]: " -i y -e DO_INSTALL_LARAVEL
done

if [[ "$DO_INSTALL_LARAVEL" != "Y" && "$DO_INSTALL_LARAVEL" != "y" ]]; then
    throw_info "Laravel server installation skipped."
fi

install_laravel